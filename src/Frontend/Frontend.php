<?php namespace Codeable\RealEstateSubscribers\Frontend;

use Premmerce\SDK\V2\FileManager\FileManager;
use function Clue\StreamFilter\fun;

/**
 * Class Frontend
 *
 * @package Codeable\RealEstateSubscribers\Frontend
 */
class Frontend {


	/**
	 * @var FileManager
	 */
	private $fileManager;

	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueScripts' ] );
		add_action( 'wp_footer', function () {
			$this->fileManager->includeTemplate( 'frontend/save-search-modal.php' );
		} );

	}

	public function enqueueScripts() {
		wp_enqueue_script( 'blockUI', $this->fileManager->locateAsset( 'frontend/blockUI.min.js' ), [ 'jquery' ] );

		wp_enqueue_script( 'real-estate-js', $this->fileManager->locateAsset( 'frontend/main.js' ), [
			'jquery',
			'blockUI'
		] );
		wp_enqueue_script( 'real-estate-magnific', $this->fileManager->locateAsset( 'frontend/magnific.min.js' ), [ 'jquery' ] );

		wp_enqueue_style( 'real-estate-magnific-css', $this->fileManager->locateAsset( 'frontend/magnific.css' ) );
		wp_enqueue_style( 'real-estate-css', $this->fileManager->locateAsset( 'frontend/main.css' ) );
	}

}