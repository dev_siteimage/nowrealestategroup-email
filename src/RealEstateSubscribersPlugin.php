<?php namespace Codeable\RealEstateSubscribers;

use Codeable\RealEstateSubscribers\Emails\RealEstateSubscriptionEmail;
use Codeable\RealEstateSubscribers\Subscriptions\Subscription;
use Codeable\RealEstateSubscribers\Subscriptions\SubscriptionCPT;
use Codeable\RealEstateSubscribers\Subscriptions\SubscriptionManager;
use Premmerce\SDK\V2\FileManager\FileManager;
use Codeable\RealEstateSubscribers\Admin\Admin;
use Codeable\RealEstateSubscribers\Frontend\Frontend;

/**
 * Class RealEstateSubscribersPlugin
 *
 * @package Codeable\RealEstateSubscribers
 */
class RealEstateSubscribersPlugin {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * RealEstateSubscribersPlugin constructor.
	 *
	 * @param string $mainFile
	 */
	public function __construct( $mainFile ) {
		$this->fileManager = new FileManager( $mainFile );

		add_action( 'plugins_loaded', [ $this, 'loadTextDomain' ] );

		new ContactForm7Manager( $this->fileManager );
		new SubscriptionCPT( $this->fileManager );
		new SubscriptionManager( $this->fileManager );
	}

	/**
	 * Run plugin part
	 */
	public function run() {
		if ( is_admin() ) {
			new Admin( $this->fileManager );
		} else {
			new Frontend( $this->fileManager );
		}
	}

	/**
	 * Load plugin translations
	 */
	public function loadTextDomain() {
		$name = $this->fileManager->getPluginName();
		load_plugin_textdomain( 'real-estate-subscribers', false, $name . '/languages/' );
	}

	/**
	 * Fired when the plugin is activated
	 */
	public function activate() {
		// TODO: Implement activate() method.
	}

	/**
	 * Fired when the plugin is deactivated
	 */
	public function deactivate() {
		// TODO: Implement deactivate() method.
	}

	/**
	 * Fired during plugin uninstall
	 */
	public static function uninstall() {
		// TODO: Implement uninstall() method.
	}
}