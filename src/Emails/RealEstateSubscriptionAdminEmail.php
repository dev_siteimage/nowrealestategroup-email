<?php namespace Codeable\RealEstateSubscribers\Emails;

use Codeable\RealEstateSubscribers\Subscriptions\Subscription;
use Premmerce\SDK\V2\FileManager\FileManager;
use WP_Query;

class RealEstateSubscriptionAdminEmail {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * RealEstateSubscriptionEmail constructor.
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;
	}

	public function sent( Subscription $subscription ) {
		$status = wp_mail( get_option('admin_email'), 'New saved search placed', $this->fileManager->renderTemplate( 'emails/real-estate-subscription-admin.php', [
			'subscription' => $subscription,
		] ), [ 'Content-Type: text/html; charset=UTF-8' ] );

		if ( $status ) {
			$subscription->log( 'Admin email notification has been sent. ' . date( 'Y-m-d H:i:s' ), 'email_sent' );
		}
	}
}