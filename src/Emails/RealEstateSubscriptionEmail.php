<?php namespace Codeable\RealEstateSubscribers\Emails;

use Codeable\RealEstateSubscribers\Subscriptions\Subscription;
use Premmerce\SDK\V2\FileManager\FileManager;
use WP_Query;

class RealEstateSubscriptionEmail {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * RealEstateSubscriptionEmail constructor.
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;
	}

	public function sent( Subscription $subscription ) {
		if ( $subscription->isActive() ) {
			$estateQuery = $subscription->getMatchedEstatesQuery();

			if ( $estateQuery && ! empty( (array) $estateQuery['posts'] ) ) {


				$viewMoreLink = add_query_arg( [
					'input_property_type'    => $subscription->getPropertyType(),
					'input_transaction_type' => $subscription->getTransactionType(),
					'input_street_address'   => $subscription->getStreetName(),
					'input_city'             => $subscription->getCity(),
					'input_province'         => $subscription->getProvince(),
					'input_postal_code'      => $subscription->getPostalCode(),
					'input_bedrooms'         => $subscription->getBedrooms(),
					'input_price'            => $subscription->getPrice(),
					'input_mls'              => $subscription->getMLS(),

					'input_pool'        => $subscription->getPool() ? $subscription->getPool() : '',
					'input_condominium' => $subscription->getCondominium() ? $subscription->getCondominium() : '',
					'input_open_house'  => $subscription->getOpenHouse() ? $subscription->getOpenHouse() : '',

					'input_neighbourhood' => $subscription->getNeighborhood(),
					'input_building_type' => $subscription->getBuildingType()
				], home_url( 'listings' ) );

				$status = wp_mail( $subscription->getEmail(), 'We found a home that may interest you', $this->fileManager->renderTemplate( 'emails/real-estate-subscription.php', [
					'subscription'   => $subscription,
					'query'          => $estateQuery,
					'view_more_link' => $viewMoreLink,
				] ), [ 'Content-Type: text/html; charset=UTF-8' ] );

				if ( $status ) {
					$subscription->log( 'Email has been sent. ' . date( 'Y-m-d H:i:s' ) . ' - found ' . $estateQuery['found_posts'] . ' items', 'email_sent' );
				}
			} else {
				$subscription->log( 'There are matched estates found. Email has not been sent ' . date( 'Y-m-d H:i:s' ), 'email_sent' );
			}
		}
	}
}