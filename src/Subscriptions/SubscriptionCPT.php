<?php namespace Codeable\RealEstateSubscribers\Subscriptions;

use Premmerce\SDK\V2\FileManager\FileManager;

class SubscriptionCPT {

	const POST_TYPE = 'real-estate-subs';
	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * SubscriptionCPT constructor.
	 *
	 * @param FileManager $fileManager
	 */

	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action( 'init', [ $this, 'register' ] );
		add_action( 'admin_head', function () {

			if ( isset( $_GET['post_type'] ) && $_GET['post_type'] == self::POST_TYPE ) {
				?>
                <style type="text/css">
                    .page-title-action {
                        display: none;
                    }
                </style>
				<?php
			}
		} );

		add_action( 'add_meta_boxes', [ $this, 'registerMetaboxes' ] );

		add_action( 'save_post_' . self::POST_TYPE, function ( $postId ) {
			if ( is_admin() ) {
				$subscription = Subscription::getByID( $postId );
				if ( $subscription ) {
					$subscription->saveFromAdminPost();
				}
			}
		} );
	}

	/**
	 *
	 */
	public function register() {
		$this->registerCTP();
	}

	public function registerMetaboxes() {
		add_meta_box( self::POST_TYPE . '_data', 'Subscription information', function ( $postId ) {
			$subscription = Subscription::getByID( $postId );

			$this->fileManager->includeTemplate( 'admin/subscription-information-metabox.php', [
				'subscription' => $subscription,
			] );
		}, self::POST_TYPE );

		add_meta_box( self::POST_TYPE . '_is_active', 'Is Active', function ( $postId ) {

			$subscription = Subscription::getByID( $postId );

			$this->fileManager->includeTemplate( 'admin/subscription-is-active-metabox.php', [
				'subscription' => $subscription,
			] );

		}, self::POST_TYPE, 'side' );

		add_meta_box( self::POST_TYPE . '_activity', 'Activity', function ( $postId ) {

			$subscription = Subscription::getByID( $postId );

			$this->fileManager->includeTemplate( 'admin/subscription-log.php', [
				'subscription' => $subscription,
			] );

		}, self::POST_TYPE, 'side' );
	}

	/**
	 * Register method
	 */
	public function registerCTP() {
		register_post_type( self::POST_TYPE, [
			'labels'             => [
				'name'               => __( 'Real estate Subscribers', 'real-estate-subscribers' ),
				'singular_name'      => __( 'Subscriber', 'real-estate-subscribers' ),
				'add_new'            => __( 'Add new', 'real-estate-subscribers' ),
				'add_new_item'       => __( 'Add new subscriber', 'real-estate-subscribers' ),
				'edit_item'          => __( 'Edit subscriber', 'real-estate-subscribers' ),
				'new_item'           => __( 'New subscriber', 'real-estate-subscribers' ),
				'view_item'          => __( 'View subscriber', 'real-estate-subscribers' ),
				'search_items'       => __( 'Find subscriber', 'real-estate-subscribers' ),
				'not_found'          => __( 'Subscriber has not been found', 'real-estate-subscribers' ),
				'not_found_in_trash' => __( 'Not found in trash', 'real-estate-subscribers' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'RE Subscribers', 'real-estate-subscribers' ),
			],
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'rewrite'            => false,
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title' ],
			'menu_icon'          => 'dashicons-backup'
		] );
	}
}