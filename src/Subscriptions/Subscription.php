<?php namespace Codeable\RealEstateSubscribers\Subscriptions;

use RealtyPress_Listings;

class Subscription {
	private $id;
	private $firstName;
	private $lastName;
	private $email;
	private $message;
	private $phone = null;
	private $country = null;
	private $city = null;
	private $propertyType = null;
	private $transactionType = null;
	private $buildingType = null;
	private $bedrooms = null;
	private $price = null;
	private $streetName = null;
	private $communityName = null;
	private $neighborhood = null;
	private $province = null;
	private $postalCode = null;
	private $MLS = null;
	private $condominium = null;
	private $pool = null;
	private $openHouse = null;
	private $hash = null;
	private $isActive = null;

	/**
	 * Subscription constructor.
	 *
	 * @param $firstName
	 * @param $lastName
	 * @param $email
	 * @param $message
	 * @param null $id
	 */
	public function __construct( $firstName, $lastName, $email, $message, $id = null ) {
		$this->firstName = $firstName;
		$this->lastName  = $lastName;
		$this->email     = $email;
		$this->message   = $message;
		$this->id        = $id;
	}

	/**
	 * @return null
	 */
	public function isActive() {
		return $this->isActive;
	}

	/**
	 * @param null $isActive
	 */
	public function setActive( $isActive ) {
		$this->isActive = (bool) $isActive;
	}

	/**
	 * @return mixed
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param mixed $firstName
	 */
	public function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	/**
	 * @return mixed
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param mixed $lastName
	 */
	public function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param mixed $message
	 */
	public function setMessage( $message ) {
		$this->message = $message;
	}

	/**
	 * @return null
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param null $phone
	 */
	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	/**
	 * @return null
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param null $country
	 */
	public function setCountry( $country ) {
		$this->country = $country;
	}

	/**
	 * @return null
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param null $city
	 */
	public function setCity( $city ) {
		$this->city = $city;
	}

	/**
	 * @return null
	 */
	public function getPropertyType() {
		return $this->propertyType;
	}

	/**
	 * @param null $propertyType
	 */
	public function setPropertyType( $propertyType ) {
		$this->propertyType = $propertyType;
	}

	/**
	 * @return null
	 */
	public function getTransactionType() {
		return $this->transactionType;
	}

	/**
	 * @param null $transactionType
	 */
	public function setTransactionType( $transactionType ) {
		$this->transactionType = $transactionType;
	}

	/**
	 * @return null
	 */
	public function getBuildingType() {
		return $this->buildingType;
	}

	/**
	 * @param null $buildingType
	 */
	public function setBuildingType( $buildingType ) {
		$this->buildingType = $buildingType;
	}

	/**
	 * @return null
	 */
	public function getBedrooms() {
		return $this->bedrooms;
	}

	/**
	 * @param null $bedrooms
	 */
	public function setBedrooms( $bedrooms ) {
		$this->bedrooms = $bedrooms;
	}

	/**
	 * @return null
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param null $price
	 */
	public function setPrice( $price ) {
		$this->price = $price;
	}

	/**
	 * @return null
	 */
	public function getStreetName() {
		return $this->streetName;
	}

	/**
	 * @param null $streetName
	 */
	public function setStreetName( $streetName ) {
		$this->streetName = $streetName;
	}

	/**
	 * @return null
	 */
	public function getCommunityName() {
		return $this->communityName;
	}

	/**
	 * @param null $communityName
	 */
	public function setCommunityName( $communityName ) {
		$this->communityName = $communityName;
	}

	/**
	 * @return null
	 */
	public function getNeighborhood() {
		return $this->neighborhood;
	}

	/**
	 * @param null $neighborhood
	 */
	public function setNeighborhood( $neighborhood ) {
		$this->neighborhood = $neighborhood;
	}

	/**
	 * @return null
	 */
	public function getProvince() {
		return $this->province;
	}

	/**
	 * @param null $province
	 */
	public function setProvince( $province ) {
		$this->province = $province;
	}

	/**
	 * @return null
	 */
	public function getPostalCode() {
		return $this->postalCode;
	}

	/**
	 * @param null $postalCode
	 */
	public function setPostalCode( $postalCode ) {
		$this->postalCode = $postalCode;
	}

	/**
	 * @return null
	 */
	public function getMLS() {
		return $this->MLS;
	}

	/**
	 * @param null $MLS
	 */
	public function setMLS( $MLS ) {
		$this->MLS = $MLS;
	}

	/**
	 * @return null
	 */
	public function getCondominium() {
		return $this->condominium;
	}

	/**
	 * @param null $condominium
	 */
	public function setCondominium( $condominium ) {
		$this->condominium = $condominium;
	}

	/**
	 * @return null
	 */
	public function getPool() {
		return $this->pool;
	}

	/**
	 * @param null $pool
	 */
	public function setPool( $pool ) {
		$this->pool = $pool;
	}

	/**
	 * @return null
	 */
	public function getOpenHouse() {
		return $this->openHouse;
	}

	/**
	 * @param null $openHouse
	 */
	public function setOpenHouse( $openHouse ) {
		$this->openHouse = $openHouse;
	}

	/**
	 * @return null
	 */
	public function getHash() {
		return $this->hash;
	}

	/**
	 * @param null $hash
	 */
	public function setHash( $hash ) {
		$this->hash = $hash;
	}

	/**
	 * @return null
	 */
	public function getId() {
		return $this->id;
	}

	public static function createFromPOST() {

		$firstName = isset( $_POST['first-name'] ) ? sanitize_text_field( $_POST['first-name'] ) : false;
		$lastName  = isset( $_POST['last-name'] ) ? sanitize_text_field( $_POST['last-name'] ) : false;
		$email     = isset( $_POST['your-email'] ) ? sanitize_email( $_POST['your-email'] ) : false;
		$message   = isset( $_POST['your-message'] ) ? sanitize_text_field( $_POST['your-message'] ) : 'message';

		if ( $firstName && $lastName && $email && $message ) {

			$subscription = new Subscription( $firstName, $lastName, $email, $message );

			if ( isset( $_POST['your-phone-number'] ) ) {
				$subscription->setPhone( sanitize_text_field( $_POST['your-phone-number'] ) );
			}

			if ( isset( $_POST['your-country'] ) ) {
				$subscription->setCountry( sanitize_text_field( $_POST['your-country'] ) );
			}

			if ( isset( $_POST['your-city'] ) ) {
				$subscription->setCity( sanitize_text_field( $_POST['your-city'] ) );
			}

			if ( isset( $_POST['input_property_type'] ) ) {
				$subscription->setPropertyType( sanitize_text_field( $_POST['input_property_type'] ) );
			}

			if ( isset( $_POST['input_transaction_type'] ) ) {
				$subscription->setTransactionType( sanitize_text_field( $_POST['input_transaction_type'] ) );
			}

			if ( isset( $_POST['input_building_type'] ) ) {
				$subscription->setBuildingType( sanitize_text_field( $_POST['input_building_type'] ) );
			}

			if ( isset( $_POST['input_bedrooms'] ) ) {
				$subscription->setBedrooms( sanitize_text_field( $_POST['input_bedrooms'] ) );
			}

			if ( isset( $_POST['input_price'] ) ) {
				$subscription->setPrice( sanitize_text_field( $_POST['input_price'] ) );
			}

			if ( isset( $_POST['input_street_address'] ) ) {
				$subscription->setStreetName( sanitize_text_field( $_POST['input_street_address'] ) );
			}

			if ( isset( $_POST['input_community_name'] ) ) {
				$subscription->setCommunityName( sanitize_text_field( $_POST['input_community_name'] ) );
			}

			if ( isset( $_POST['input_neighbourhood'] ) ) {
				$subscription->setNeighborhood( sanitize_text_field( $_POST['input_neighbourhood'] ) );
			}

			if ( isset( $_POST['input_province'] ) ) {
				$subscription->setProvince( sanitize_text_field( $_POST['input_province'] ) );
			}

			if ( isset( $_POST['input_postal_code'] ) ) {
				$subscription->setPostalCode( sanitize_text_field( $_POST['input_postal_code'] ) );
			}

			if ( isset( $_POST['input_mls'] ) ) {
				$subscription->setMLS( sanitize_text_field( $_POST['input_mls'] ) );
			}

			if ( isset( $_POST['input_condominium'] ) ) {
				$subscription->setCondominium( ( $_POST['input_condominium'] ) ? 1 : 0 );
			} else {
				$subscription->setCondominium( 0 );
			}

			if ( isset( $_POST['input_pool'] ) ) {
				$subscription->setPool( ( $_POST['input_pool'] ) ? 1 : 0 );
			} else {
				$subscription->setPool( 0 );
			}

			if ( isset( $_POST['input_open_house'] ) ) {
				$subscription->setOpenHouse( ( $_POST['input_open_house'] ) ? 1 : 0 );
			} else {
				$subscription->setOpenHouse( 0 );
			}

			$subscription->setActive( true );

			$subscription->create();

			$subscription->setLastEmailTime( time() );

			return $subscription;
		}

		return false;
	}

	public static function getByID( $id ) {
		$post = get_post( $id );

		if ( $post && $post->post_type === SubscriptionCPT::POST_TYPE ) {
			$postMeta = get_post_meta( $post->ID, '', true );

			$postMeta = array_map( function ( $i ) {
				return ( is_array( $i ) && isset( $i[0] ) ) ? $i[0] : $i;
			}, $postMeta );

			$firstName = isset( $postMeta['_firstName'] ) ? sanitize_text_field( $postMeta['_firstName'] ) : false;
			$lastName  = isset( $postMeta['_lastName'] ) ? sanitize_text_field( $postMeta['_lastName'] ) : false;
			$email     = isset( $postMeta['_email'] ) ? sanitize_email( $postMeta['_email'] ) : false;
			$message   = isset( $postMeta['_message'] ) ? sanitize_text_field( $postMeta['_message'] ) : false;

			if ( $firstName && $lastName && $email && $message ) {
				$subscription = new self( $firstName, $lastName, $email, $message, $post->ID );

				if ( isset( $postMeta['_phone'] ) ) {
					$subscription->setPhone( sanitize_text_field( $postMeta['_phone'] ) );
				}

				if ( isset( $postMeta['_country'] ) ) {
					$subscription->setCountry( sanitize_text_field( $postMeta['_country'] ) );
				}

				if ( isset( $postMeta['_city'] ) ) {
					$subscription->setCity( sanitize_text_field( $postMeta['_city'] ) );
				}

				if ( isset( $postMeta['_propertyType'] ) ) {
					$subscription->setPropertyType( sanitize_text_field( $postMeta['_propertyType'] ) );
				}

				if ( isset( $postMeta['_transactionType'] ) ) {
					$subscription->setTransactionType( sanitize_text_field( $postMeta['_transactionType'] ) );
				}

				if ( isset( $postMeta['_buildingType'] ) ) {
					$subscription->setBuildingType( sanitize_text_field( $postMeta['_buildingType'] ) );
				}

				if ( isset( $postMeta['_bedrooms'] ) ) {
					$subscription->setBedrooms( sanitize_text_field( $postMeta['_bedrooms'] ) );
				}

				if ( isset( $postMeta['_price'] ) ) {
					$subscription->setPrice( sanitize_text_field( $postMeta['_price'] ) );
				}

				if ( isset( $postMeta['_streetName'] ) ) {
					$subscription->setStreetName( sanitize_text_field( $postMeta['_streetName'] ) );
				}

				if ( isset( $postMeta['_communityName'] ) ) {
					$subscription->setCommunityName( sanitize_text_field( $postMeta['_communityName'] ) );
				}

				if ( isset( $postMeta['_neighborhood'] ) ) {
					$subscription->setNeighborhood( sanitize_text_field( $postMeta['_neighborhood'] ) );
				}

				if ( isset( $postMeta['_province'] ) ) {
					$subscription->setProvince( sanitize_text_field( $postMeta['_province'] ) );
				}

				if ( isset( $postMeta['_postalCode'] ) ) {
					$subscription->setPostalCode( sanitize_text_field( $postMeta['_postalCode'] ) );
				}

				if ( isset( $postMeta['_MLS'] ) ) {
					$subscription->setMLS( sanitize_text_field( $postMeta['_MLS'] ) );
				}

				if ( isset( $postMeta['_condominium'] ) ) {
					$subscription->setCondominium( ( $postMeta['_condominium'] ) ? 1 : 0 );
				} else {
					$subscription->setCondominium( 0 );
				}

				if ( isset( $postMeta['_pool'] ) ) {
					$subscription->setPool( ( $postMeta['_pool'] ) ? 1 : 0 );
				} else {
					$subscription->setPool( 0 );
				}

				if ( isset( $postMeta['_openHouse'] ) ) {
					$subscription->setOpenHouse( ( $postMeta['_openHouse'] ) ? 1 : 0 );
				} else {
					$subscription->setOpenHouse( 0 );
				}

				if ( isset( $postMeta['_hash'] ) ) {
					$subscription->setHash( $postMeta['_hash'] );
				}

				if ( isset( $postMeta['_is_active'] ) ) {
					$subscription->setActive( $postMeta['_is_active'] === 'yes' );
				} else {
					$subscription->setActive( false );
				}

				return $subscription;
			}
		}

		return false;
	}

	public function getMatchedEstatesQuery() {

		$data = array(
			'input_property_type'    => $this->getPropertyType(),
			'input_transaction_type' => $this->getTransactionType(),
			'input_street_address'   => $this->getStreetName(),
			'input_city'             => $this->getCity(),
			'input_province'         => $this->getProvince(),
			'input_postal_code'      => $this->getPostalCode(),
			'input_bedrooms'         => $this->getBedrooms(),
			'input_price'            => $this->getPrice(),
			'input_mls'              => $this->getMLS(),

			'input_pool'        => $this->getPool() ? $this->getPool() : '',
			'input_condominium' => $this->getCondominium() ? $this->getCondominium() : '',
			'input_open_house'  => $this->getOpenHouse() ? $this->getOpenHouse() : '',

			'input_neighbourhood' => $this->getNeighborhood(),
			'input_building_type' => $this->getBuildingType()
		);

		$data['paged']          = 1;
		$data['view']           = 'grid';
		$data['posts_per_page'] = 6;

		$list = new RealtyPress_Listings();

		$query          = (array) $list->rps_search_posts( $data );
		$query['posts'] = array_map( function ( $item ) {
			return get_post( $item->ID );
		}, (array) $query['posts'] );

		return $query;
	}

	public function getLastEmailTime() {
		$timestamp = (int) get_post_meta( $this->getId(), '_last_email_time', true );

		return $timestamp ? $timestamp : time() - DAY_IN_SECONDS;
	}

	public function setLastEmailTime( $time ) {
		update_post_meta( $this->getId(), '_last_email_time', $time );
	}

	public function getLog() {
		return ( array ) get_post_meta( $this->getId(), '_subscription_log', true );
	}

	public function setLog( $log ) {
		update_post_meta( $this->getId(), '_subscription_log', (array) $log );
	}

	public function log( $message, $type ) {
		$log = $this->getLog();

		$log[] = [
			'message' => $message,
			'type'    => $type,
		];

		$this->setLog( $log );
	}

	public function generateHash() {
		return wp_hash( $this->getFirstName() . time() );
	}

	public function getUnsubscribeLink() {
		return add_query_arg( [
			'real_estate_unsubscribe' => $this->getHash(),
		], home_url() );
	}

	public function create() {
		if ( $this->id ) {
			return;
		}

		$id = wp_insert_post( [
			'post_type'   => SubscriptionCPT::POST_TYPE,
			'post_title'  => $this->getFirstName() . ' ' . $this->getLastName(),
			'post_status' => 'publish',
		], true );

		if ( $id instanceof \WP_Error ) {
			// Log
		} else {
			update_post_meta( $id, '_firstName', $this->getFirstName() );
			update_post_meta( $id, '_lastName', $this->getLastName() );
			update_post_meta( $id, '_email', $this->getEmail() );
			update_post_meta( $id, '_message', $this->getMessage() );
			update_post_meta( $id, '_phone', $this->getPhone() );
			update_post_meta( $id, '_country', $this->getCountry() );
			update_post_meta( $id, '_city', $this->getCity() );
			update_post_meta( $id, '_propertyType', $this->getPropertyType() );
			update_post_meta( $id, '_transactionType', $this->getTransactionType() );
			update_post_meta( $id, '_buildingType', $this->getBuildingType() );
			update_post_meta( $id, '_bedrooms', $this->getBedrooms() );
			update_post_meta( $id, '_price', $this->getPrice() );
			update_post_meta( $id, '_streetName', $this->getStreetName() );
			update_post_meta( $id, '_communityName', $this->getCommunityName() );
			update_post_meta( $id, '_neighborhood', $this->getNeighborhood() );
			update_post_meta( $id, '_province', $this->getProvince() );
			update_post_meta( $id, '_postalCode', $this->getPostalCode() );
			update_post_meta( $id, '_MLS', $this->getMLS() );
			update_post_meta( $id, '_condominium', $this->getCondominium() );
			update_post_meta( $id, '_pool', $this->getPool() );
			update_post_meta( $id, '_openHouse', $this->getOpenHouse() );
			update_post_meta( $id, '_hash', $this->generateHash() );
			update_post_meta( $id, '_is_active', $this->isActive() ? 'yes' : 'no' );

			$this->id = $id;
		}
	}

	public function saveFromAdminPost() {
		$this->setFirstName( isset( $_POST['first_name'] ) ? sanitize_text_field( $_POST['first_name'] ) : '' );
		$this->setLastName( isset( $_POST['last_name'] ) ? sanitize_text_field( $_POST['last_name'] ) : '' );
		$this->setEmail( isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : '' );
		$this->setMessage( isset( $_POST['message'] ) ? sanitize_text_field( $_POST['message'] ) : '' );
		$this->setPhone( isset( $_POST['phone'] ) ? sanitize_text_field( $_POST['phone'] ) : '' );
		$this->setCountry( isset( $_POST['country'] ) ? sanitize_text_field( $_POST['country'] ) : '' );
		$this->setCity( isset( $_POST['city'] ) ? sanitize_text_field( $_POST['city'] ) : '' );
		$this->setPropertyType( isset( $_POST['property_type'] ) ? sanitize_text_field( $_POST['property_type'] ) : '' );
		$this->setTransactionType( isset( $_POST['transaction_type'] ) ? sanitize_text_field( $_POST['transaction_type'] ) : '' );
		$this->setBuildingType( isset( $_POST['building_type'] ) ? sanitize_text_field( $_POST['building_type'] ) : '' );
		$this->setBedrooms( isset( $_POST['bedrooms'] ) ? sanitize_text_field( $_POST['bedrooms'] ) : '' );
		$this->setPrice( isset( $_POST['price'] ) ? sanitize_text_field( $_POST['price'] ) : '' );
		$this->setStreetName( isset( $_POST['street_name'] ) ? sanitize_text_field( $_POST['street_name'] ) : '' );
		$this->setCommunityName( isset( $_POST['community_name'] ) ? sanitize_text_field( $_POST['community_name'] ) : '' );
		$this->setNeighborhood( isset( $_POST['neighborhood'] ) ? sanitize_text_field( $_POST['neighborhood'] ) : '' );
		$this->setProvince( isset( $_POST['province'] ) ? sanitize_text_field( $_POST['province'] ) : '' );
		$this->setPostalCode( isset( $_POST['postal_code'] ) ? sanitize_text_field( $_POST['postal_code'] ) : '' );
		$this->setMLS( isset( $_POST['mls'] ) ? sanitize_text_field( $_POST['mls'] ) : '' );

		$this->setCondominium( isset( $_POST['postal_code'] ) ? isset( $_POST['condominium'] ) ? 1 : 0 : 0 );
		$this->setPool( isset( $_POST['pool'] ) ? isset( $_POST['pool'] ) ? 1 : 0 : 0 );
		$this->setOpenHouse( isset( $_POST['open_house'] ) ? isset( $_POST['open_house'] ) ? 1 : 0 : 0 );

		$this->setActive( isset( $_POST['_is_active'] ) ? 'yes' : 'no' );

		$this->save();
	}

	public function save() {
		if ( $this->getId() ) {
			update_post_meta( $this->getId(), '_firstName', $this->getFirstName() );
			update_post_meta( $this->getId(), '_lastName', $this->getLastName() );
			update_post_meta( $this->getId(), '_email', $this->getEmail() );
			update_post_meta( $this->getId(), '_message', $this->getMessage() );
			update_post_meta( $this->getId(), '_phone', $this->getPhone() );
			update_post_meta( $this->getId(), '_country', $this->getCountry() );
			update_post_meta( $this->getId(), '_city', $this->getCity() );
			update_post_meta( $this->getId(), '_propertyType', $this->getPropertyType() );
			update_post_meta( $this->getId(), '_transactionType', $this->getTransactionType() );
			update_post_meta( $this->getId(), '_buildingType', $this->getBuildingType() );
			update_post_meta( $this->getId(), '_bedrooms', $this->getBedrooms() );
			update_post_meta( $this->getId(), '_price', $this->getPrice() );
			update_post_meta( $this->getId(), '_streetName', $this->getStreetName() );
			update_post_meta( $this->getId(), '_communityName', $this->getCommunityName() );
			update_post_meta( $this->getId(), '_neighborhood', $this->getNeighborhood() );
			update_post_meta( $this->getId(), '_province', $this->getProvince() );
			update_post_meta( $this->getId(), '_postalCode', $this->getPostalCode() );
			update_post_meta( $this->getId(), '_MLS', $this->getMLS() );
			update_post_meta( $this->getId(), '_condominium', $this->getCondominium() );
			update_post_meta( $this->getId(), '_pool', $this->getPool() );
			update_post_meta( $this->getId(), '_openHouse', $this->getOpenHouse() );
			update_post_meta( $this->getId(), '_is_active', $this->isActive() ? 'yes' : 'no' );

			$lastEmailTime = $this->getLastEmailTime();

			if ( ! $lastEmailTime ) {
				$this->setLastEmailTime( time() - DAY_IN_SECONDS );
			}
		}
	}
}