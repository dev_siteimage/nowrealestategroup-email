<?php namespace Codeable\RealEstateSubscribers\Subscriptions;

use Codeable\RealEstateSubscribers\Emails\RealEstateSubscriptionAdminEmail;
use Codeable\RealEstateSubscribers\Emails\RealEstateSubscriptionEmail;
use Premmerce\SDK\V2\FileManager\FileManager;
use WP_Query;

class SubscriptionManager {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * SubscriptionCPT constructor.
	 *
	 * @param FileManager $fileManager
	 */

	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action( 'init', array( $this, 'checkForUnsubscribe' ) );
		add_action( 'init', array( $this, 'sendScheduledEmails' ) );

		add_action( 'admin_post_create_subscription_from_search', [ $this, 'createFromSearch' ] );
		add_action( 'admin_post_nopriv_create_subscription_from_search', [ $this, 'createFromSearch' ] );
		add_action( 'admin_post_real_estate_send_email', [ $this, 'sendSubscriptionEmail' ] );
	}

	public function sendSubscriptionEmail() {
		$subscriptionId = isset( $_GET['subscription_id'] ) ? (int) $_GET['subscription_id'] : 0;

		if ( $subscriptionId ) {
			$subscription = Subscription::getByID( $subscriptionId );

			if ( $subscription ) {
				$email = new RealEstateSubscriptionEmail( $this->fileManager );
				$email->sent( $subscription );
				$subscription->setLastEmailTime( time() );

			}
		}

		wp_safe_redirect( wp_get_referer() );
	}

	public function createFromSearch() {

		if ( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'create_subscription_from_search' ) ) {

			$subscription = Subscription::createFromPOST();

			if ( $subscription instanceof Subscription ) {

				$adminEmail = new RealEstateSubscriptionAdminEmail( $this->fileManager );

				$adminEmail->sent( $subscription );

				return wp_send_json( [
					'success' => true,
				] );
			}
		}

		return wp_send_json( [
			'success' => false,
		] );
	}

	public function checkForUnsubscribe() {
		if ( isset( $_GET['real_estate_unsubscribe'] ) ) {
			$key = sanitize_text_field( $_GET['real_estate_unsubscribe'] );

			$args = array(
				'post_type'  => SubscriptionCPT::POST_TYPE,
				'meta_query' => array(
					array(
						'key'     => '_hash',
						'value'   => $key,
						'compare' => '=',
					)
				)
			);

			$posts = get_posts( $args );

			if ( ! empty( $posts[0] ) ) {
				$subscription = $posts[0] instanceof \WP_Post ? Subscription::getByID( $posts[0]->ID ) : false;

				if ( ! $subscription->isActive() ) {
					wp_redirect( home_url() );
				}

				if ( $subscription ) {
					update_post_meta( $subscription->getId(), '_is_active', 'no' );

					?>
                    <h1>You have been successfully unsubscribed. </h1>
					<?php

					die;
				}
			}
		}
	}

	public function sendScheduledEmails() {
		$subscriptions = new WP_Query( [
			'post_type'      => SubscriptionCPT::POST_TYPE,
			'posts_per_page' => 3,
			'post_status'    => 'publish',
			'meta_query'     => [
				'relation' => 'OR',
				[
					'key'     => '_last_email_time',
					'compare' => 'NOT EXISTS'
				],
				[
					'key'     => '_last_email_time',
					'value'   => time() - DAY_IN_SECONDS,
					'compare' => '<=',
				]
			],
		] );

		foreach ( $subscriptions->posts as $subscription ) {

			$subscription = Subscription::getByID( $subscription->ID );

			if ( $subscription ) {
				$email = new RealEstateSubscriptionEmail( $this->fileManager );
				$email->sent( $subscription );
				$subscription->setLastEmailTime( time() );
			}
		}
	}
}