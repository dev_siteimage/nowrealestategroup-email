<?php namespace Codeable\RealEstateSubscribers;

use Codeable\RealEstateSubscribers\Subscriptions\Subscription;
use Premmerce\SDK\V2\FileManager\FileManager;

/**
 * Class RealEstateSubscribersPlugin
 *
 * @package Codeable\RealEstateSubscribers
 */
class ContactForm7Manager {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	const CONTACT_US_FORM_ID = 57809;
	const BUYERS_FORM_ID = 58292;

	/**
	 * RealEstateSubscribersPlugin constructor.
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action( 'wpcf7_init', function () {
			wpcf7_add_form_tag( 'real_estate_search_bar', function () {
				return $this->fileManager->renderTemplate( 'frontend/cf7-search-bar-form.php' );
			} );
		} );

		add_action( 'wpcf7_mail_sent', function ( $instance ) {

			if ( $instance instanceof \WPCF7_ContactForm && ( $instance->id() === self::CONTACT_US_FORM_ID || $instance->id() === self::BUYERS_FORM_ID) ) {
				Subscription::createFromPOST();
			}
		} );

	}
}