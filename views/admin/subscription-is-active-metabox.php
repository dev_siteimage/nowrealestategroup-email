<?php use Codeable\RealEstateSubscribers\Subscriptions\Subscription;

defined( "ABSPATH" ) || die;

/**
 * @var Subscription $subscription
 */

?>

<?php if ( $subscription ): ?>
    <div>
        <label for="">
            Is Active
            <input type="checkbox" name="_is_active" <?php checked( $subscription->isActive() ) ?>>
        </label>

    </div>
<?php endif; ?>
