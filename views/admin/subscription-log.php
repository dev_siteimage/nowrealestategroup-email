<?php use Codeable\RealEstateSubscribers\Subscriptions\Subscription;

defined( "ABSPATH" ) || die;

/**
 * @var Subscription $subscription
 */

?>

<?php if ( $subscription ): ?>
    <div>
        <p><a href="<?php echo add_query_arg( [
				'action'          => 'real_estate_send_email',
				'subscription_id' => $subscription->getId(),
				'nonce'           => wp_create_nonce( 'real_estate_send_email' )
			], admin_url( 'admin-post.php' ) ) ?>">Send Email Now</a>
        </p>
        <p>Last email time: <?php echo date( 'Y-m-d H:i:s', $subscription->getLastEmailTime() ); ?></p>
    </div>

	<?php if ( ! empty( $subscription->getLog() ) ): ?>
		<?php

		$log = array_slice( $subscription->getLog(), 0, 30 );
		$subscription->setLog( $log );

		foreach ( array_reverse( $log ) as $logItem ): ?>
			<?php if ( $logItem ): ?>
                <div style="height: 40px; background: #a6c4e9; color: #000; padding: 10px; margin-bottom: 15px">
					<?php echo $logItem['message']; ?>
                </div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>
