<?php use Codeable\RealEstateSubscribers\Subscriptions\Subscription;

defined( "ABSPATH" ) || die;

/**
 * @var Subscription $subscription
 */

?>

<?php if ( $subscription ): ?>
    <div>
        <style>
            .real-estate_subscription_data_item {
                margin: 10px 0;
                display: flex;
            }

            .real-estate_subscription_data_item__title {
                min-width: 130px
            }
        </style>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>First Name:</b>
            </div>
            <div>
                <input type="text" name="first_name" value="<?php echo $subscription->getFirstName(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Last Name:</b>
            </div>
            <div>
                <input type="text" name="last_name" value="<?php echo $subscription->getLastName(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Email:</b>
            </div>
            <div>
                <input type="text" name="email" value="<?php echo $subscription->getEmail(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Message:</b>
            </div>
            <div>
                <textarea name="message" cols="30" rows="4"><?php echo $subscription->getMessage(); ?></textarea>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Phone:</b>
            </div>
            <div>
                <input type="text" name="phone" value="<?php echo $subscription->getPhone(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Country:</b>
            </div>
            <div>
                <input type="text" name="country" value="<?php echo $subscription->getCountry(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Property type:</b>
            </div>
            <div>
                <select name="property_type" id="input_property_type" class="form-control ">
                    <option value="" <?php selected( $subscription->getPropertyType(), '' ); ?>>All Property Types
                    </option>
                    <option value="Agriculture" <?php selected( $subscription->getPropertyType(), 'Agriculture' ); ?>>
                        Agriculture
                    </option>
                    <option value="Business" <?php selected( $subscription->getPropertyType(), 'Business' ); ?>>Business
                    </option>
                    <option value="Industrial" <?php selected( $subscription->getPropertyType(), 'Industrial' ); ?>>
                        Industrial
                    </option>
                    <option value="Institutional - Special Purpose" <?php selected( $subscription->getPropertyType(), 'Institutional - Special Purpose' ); ?>>
                        Institutional - Special Purpose
                    </option>
                    <option value="Multi-family" <?php selected( $subscription->getPropertyType(), 'Multi-family"' ); ?>>
                        Multi-family
                    </option>
                    <option value="Office" <?php selected( $subscription->getPropertyType(), 'Office' ); ?>>Office
                    </option>
                    <option value="Recreational" <?php selected( $subscription->getPropertyType(), 'Recreational' ); ?>>
                        Recreational
                    </option>
                    <option value="Retail" <?php selected( $subscription->getPropertyType(), 'Retail' ); ?>>Retail
                    </option>
                    <option value="Single Family" <?php selected( $subscription->getPropertyType(), 'Single Family' ); ?>>
                        Single
                        Family
                    </option>
                    <option value="Vacant Land" <?php selected( $subscription->getPropertyType(), 'Vacant Land' ); ?>>
                        Vacant Land
                    </option>
                </select>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Transaction type:</b>
            </div>
            <div>
                <select name="transaction_type" id="input_transaction_type" class="form-control ">
                    <option value="" <?php selected( $subscription->getTransactionType(), '' ); ?>>All Transaction
                        Types
                    </option>
                    <option value="for lease" <?php selected( $subscription->getTransactionType(), 'for lease' ); ?>>For
                        Lease
                    </option>
                    <option value="for sale" <?php selected( $subscription->getTransactionType(), 'for sale' ); ?>>For
                        Sale
                    </option>
                    <option value="for sale or rent" <?php selected( $subscription->getTransactionType(), 'for sale or rent' ); ?>>
                        For
                        Sale Or Rent
                    </option>
                </select>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Building type:</b>
            </div>
            <div>
                <select name="building_type" id="input_building_type" class="form-control ">
                    <option value="" <?php selected( $subscription->getBuildingType(), '' ); ?>>All Building Types
                    </option>
                    <option value="Apartment" <?php selected( $subscription->getBuildingType(), 'Apartment' ); ?>>
                        Apartment
                    </option>
                    <option value="Duplex" <?php selected( $subscription->getBuildingType(), 'Duplex' ); ?>>Duplex
                    </option>
                    <option value="Fourplex" <?php selected( $subscription->getBuildingType(), 'Fourplex' ); ?>>Fourplex
                    </option>
                    <option value="House" <?php selected( $subscription->getBuildingType(), 'House' ); ?>>House</option>
                    <option value="Manufactured Home" <?php selected( $subscription->getBuildingType(), 'Manufactured Home' ); ?>>
                        Manufactured Home
                    </option>
                    <option value="Mobile Home" <?php selected( $subscription->getBuildingType(), 'Mobile Home' ); ?>>
                        Mobile Home
                    </option>
                    <option value="No Building" <?php selected( $subscription->getBuildingType(), 'No Building' ); ?>>No
                        Building
                    </option>
                    <option value="Parking" <?php selected( $subscription->getBuildingType(), 'Parking' ); ?>>Parking
                    </option>
                    <option value="Recreational" <?php selected( $subscription->getBuildingType(), 'Recreational' ); ?>>
                        Recreational
                    </option>
                    <option value="Row / Townhouse" <?php selected( $subscription->getBuildingType(), 'Row /
                        Townhouse' ); ?>>Row /
                        Townhouse
                    </option>
                    <option value="Triplex" <?php selected( $subscription->getBuildingType(), 'Triplex' ); ?>>Triplex
                    </option>
                </select>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Bedrooms:</b>
            </div>
            <div>
                <input type="text" name="bedrooms" value="<?php echo $subscription->getBedrooms(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Price:</b>
            </div>
            <div>
                <input type="text" name="price" value="<?php echo $subscription->getPrice(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Street:</b>
            </div>
            <div>
                <input type="text" name="street_name" value="<?php echo $subscription->getStreetName(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Community name:</b>
            </div>
            <div>
                <input type="text" name="community_name" value="<?php echo $subscription->getCommunityName(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Neighbourhood:</b>
            </div>
            <div>
                <input type="text" name="neighborhood" value="<?php echo $subscription->getNeighborhood(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Province:</b>
            </div>
            <div>
                <input type="text" name="province" value="<?php echo $subscription->getProvince(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Postal code:</b>
            </div>
            <div>
                <input type="text" name="postal_code" value="<?php echo $subscription->getPostalCode(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>MLS:</b>
            </div>
            <div>
                <input type="text" name="mls" value="<?php echo $subscription->getMLS(); ?>">
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Condominium:</b>
            </div>
            <div>
                <input type="checkbox" name="condominium" <?php checked( $subscription->getCondominium() ) ?>>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Pool:</b>
            </div>
            <div>
                <input type="checkbox" name="pool" <?php checked( $subscription->getPool() ) ?>>
            </div>
        </div>

        <div class="real-estate_subscription_data_item">
            <div class="real-estate_subscription_data_item__title">
                <b>Open House:</b>
            </div>
            <div>
                <input type="checkbox" name="open_house" <?php checked( $subscription->getOpenHouse() ) ?>>
            </div>
        </div>

    </div>
<?php endif; ?>