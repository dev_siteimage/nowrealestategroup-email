<?php use Codeable\RealEstateSubscribers\Subscriptions\Subscription;

defined( 'ABSPATH' ) || die;

/**
 * @var Subscription $subscription
 *
 **/
?>

<p><?php echo $subscription->getFirstName() . ' ' . $subscription->getLastName() ?> just <a
            href="<?php echo get_edit_post_link( $subscription->getId() ); ?>">Subscribed to the real estate searching</a>. </p>
