<?php defined( 'ABSPATH' ) || die;
?>
<div class="modal micromodal-slide" id="real-estate-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <form action="" data-real-estate-modal-form>
                <main class="modal__content">
                    <p>
                        <label>First Name<span class="required">*</span><br>
                            <span>
                            <input type="text" name="real-estate-first-name" required size="40" aria-required="true"
                                   aria-invalid="false">
                        </span>
                        </label>
                    </p>
                    <p>
                        <label>Last Name<span class="required">*</span><br>
                            <span>
                            <input type="text" name="real-estate-last-name" required size="40" aria-required="true"
                                   aria-invalid="false">
                        </span>
                        </label>
                    </p>
                    <p>
                        <label>Email Name<span class="required">*</span><br>
                            <span>
                            <input type="email" name="real-estate-email" required size="40" aria-required="true"
                                   aria-invalid="false">
                        </span>
                        </label>
                    </p>

                    <p>
                        <label>Phone number<br>
                            <span>
                            <input type="text" name="real-estate-phone" size="40" aria-required="true"
                                   aria-invalid="false">
                        </span>
                        </label>
                    </p>

                    <input type="hidden" name="real-estate-nonce"
                           value="<?php echo wp_create_nonce( 'create_subscription_from_search' ); ?>">

                    <input type="hidden" name="real-estate-post-url"
                           value="<?php echo admin_url('admin-post.php'); ?>">

                </main>

                <main class="modal__content--success" style="display: none">
                   <h4>Your search have been saved successfully.</h4>
                </main>


                <main class="modal__content--error" style="display: none">
                    <h4>Something went wrong, please try again.</h4>
                </main>

                <footer class="modal__footer">
                    <button class="button button-primary" id="real-estate-modal-save">Save search</button>
                </footer>
            </form>
        </div>
    </div>
</div>