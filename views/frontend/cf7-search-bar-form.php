<?php defined( 'ABSPATH' ) || die;

/*
 * TODO: REMOVE HARDCODE
 */

?>
<div class="bootstrap-realtypress">

    <!-- Property Type -->
    <div class="form-group">
        <select name="input_property_type" id="input_property_type" class="form-control ">
            <option value="">All Property Types</option>
            <option value="Agriculture">Agriculture</option>
            <option value="Business">Business</option>
            <option value="Industrial">Industrial</option>
            <option value="Institutional - Special Purpose">Institutional - Special Purpose</option>
            <option value="Multi-family">Multi-family</option>
            <option value="Office">Office</option>
            <option value="Recreational">Recreational</option>
            <option value="Retail">Retail</option>
            <option value="Single Family">Single Family</option>
            <option value="Vacant Land">Vacant Land</option>
        </select></div>

    <!-- Transaction Type -->
    <div class="form-group">
        <select name="input_transaction_type" id="input_transaction_type" class="form-control ">
            <option value="">All Transaction Types</option>
            <option value="for lease">For Lease</option>
            <option value="for sale">For Sale</option>
            <option value="for sale or rent">For Sale Or Rent</option>
        </select></div>


    <!-- Building Type -->
    <div class="form-group">
        <select name="input_building_type" id="input_building_type" class="form-control ">
            <option value="">All Building Types</option>
            <option value="Apartment">Apartment</option>
            <option value="Duplex">Duplex</option>
            <option value="Fourplex">Fourplex</option>
            <option value="House">House</option>
            <option value="Manufactured Home">Manufactured Home</option>
            <option value="Mobile Home">Mobile Home</option>
            <option value="No Building">No Building</option>
            <option value="Parking">Parking</option>
            <option value="Recreational">Recreational</option>
            <option value="Row / Townhouse">Row / Townhouse</option>
            <option value="Triplex">Triplex</option>
        </select></div>

    <input type="hidden" id="input_construction_style" name="input_construction_style" value="" class="form-control ">


    <!-- Bedrooms -->
    <div class="jrange-input bedroom-range">
        <label for="input_bedrooms"><small>Bedrooms</small></label>
        <div class="range">
            <input type="hidden" name="input_bedrooms" class="bed-slider-input" value="0,10" style="display: none;">
            <input type="hidden" name="input_bedrooms_max" class="bed-slider-max" value="10">
        </div>
    </div>


    <div class="jrange-input price-range">
        <label for="input_price"><small>Price</small></label>
        <div class="range">
            <input type="hidden" name="input_price" class="price-slider-input" value="0,1000000" style="display: none;">
            <input type="hidden" name="input_price_max" class="price-slider-max" value="1000000">
        </div>
    </div>

    <!-- Street Address -->
    <div class="form-group">
        <input type="text" id="input_street_address" name="input_street_address" value="" class="form-control "
               placeholder="Enter Street Name">
    </div>

    <!-- Community Name -->
    <div class="form-group">
        <input type="text" id="input_community_name" name="input_community_name" value="" class="form-control "
               placeholder="Enter Community Name">
    </div>


    <!-- Neighbourhood -->
    <div class="form-group">
        <input type="text" id="input_neighbourhood" name="input_neighbourhood" value="" class="form-control "
               placeholder="Enter Neighbourhood">
    </div>

    <!-- Province -->
    <div class="form-group">
        <select name="input_province" id="input_province" class="form-control ">
            <option value="">All Provinces</option>
            <option value="Alberta">Alberta</option>
        </select></div>

    <!-- Postal Code -->
    <div class="form-group">
        <input type="text" id="input_postal_code" name="input_postal_code" value="" class="form-control "
               placeholder="Enter Postal Code">
    </div>

    <!-- MLS Number -->
    <div class="form-group">
        <input type="text" id="input_mls" name="input_mls" value="" class="form-control " placeholder="Enter MLS®">
    </div>

    <input type="hidden" id="input_description" name="input_description" value="" class="form-control ">

    <!-- Condominium -->
    <div>
        <label for="input_condominium">
            <input type="hidden" id="input_condominium_hidden" name="input_condominium" value="0">
            <input type="checkbox" id="input_condominium" name="input_condominium" value="1"> Condominium</label>
    </div>


    <!-- Pool -->
    <div>
        <label for="input_pool">
            <input type="hidden" id="input_pool_hidden" name="input_pool" value="0">
            <input type="checkbox" id="input_pool" name="input_pool" value="1"> Pool</label>
    </div>


    <input type="hidden" id="input_waterfront" name="input_waterfront" value="" class="form-control ">


    <!-- Open House -->
    <div>
        <label for="input_open_house">
            <input type="hidden" id="input_open_house_hidden" name="input_open_house" value="0">
            <input type="checkbox" id="input_open_house" name="input_open_house" value="1"> Open House</label>
    </div>


</div>

