jQuery(document).ready(function ($) {
    $('.result-filter-frm .panel-footer').append('<button class="btn btn-large btn-block btn-result-filter" data-real-estate-save-search style="\n' +
        '    color: white;\n' +
        '">Save search</button>');


    /**
     * Open modal
     */
    $(document).on('click', '[data-real-estate-save-search]', function (e) {
        e.preventDefault();

        MicroModal.show('real-estate-modal');


    });

    $(document).on('submit', '[data-real-estate-modal-form]', function (e) {
        e.preventDefault();
        var adminPostURL = $('[name=real-estate-post-url]').val();
        var nonce = $('[name=real-estate-nonce]').val();

        var firstName = $('[name=real-estate-first-name]').val();
        var lastName = $('[name=real-estate-last-name]').val();
        var email = $('[name=real-estate-email]').val();
        var phone = $('[name=real-estate-phone]').val();

        var propertyType = $('[name=input_property_type]').val();
        var transactionType = $('[name=input_transaction_type]').val();
        var buildingType = $('[name=input_building_type]').val();

        var bedrooms = $('[name=input_bedrooms]').val();
        var bathrooms = $('[name=input_baths]').val();
        var price = $('[name=input_price]').val();

        var street = $('[name=input_street_address]').val();
        var city = $('[name=input_city]').val();
        var communityName = $('[name=input_community_name]').val();
        var neighbourhood = $('[name=input_neighbourhood]').val();
        var province = $('[name=input_province]').val();
        var postalCode = $('[name=input_postal_code]').val();
        var mls = $('[name=input_mls]').val();

        var condominium = $('[name=input_condominium]').is(':checked') ? 1 : 0;
        var pool = $('[name=input_pool]').is(':checked') ? 1 : 0;
        var openHouse = $('[name=input_open_house]').is(':checked') ? 1 : 0;

        var data = {
            'action': 'create_subscription_from_search',
            'nonce': nonce,
            'first-name': firstName,
            'last-name': lastName,
            'your-email': email,
            'input_property_type': propertyType,
            'input_transaction_type': transactionType,
            'input_building_type': buildingType,
            'input_bedrooms': bedrooms,
            'input_bathrooms': bathrooms,
            'input_price': price,
            'input_street_address': street,
            'your-city': city,
            'input_community_name': communityName,
            'input_neighbourhood': neighbourhood,
            'input_province': province,
            'input_postal_code': postalCode,
            'input_mls': mls,
            'input_condominium': condominium,
            'input_pool': pool,
            'input_open_house': openHouse,

            'your-phone-number': phone,
            'your-country': '',
        }
        var $block = $(e.target).parent();

        $block.block({message: '<p> Just a moment...</p>', css: { backgroundColor: 'transparent', color: '#fff', 'border': 'none'}});

        $.post(adminPostURL, data, function (response) {
            $('#real-estate-modal .modal__content').hide();
            $('#real-estate-modal-save').hide();

            if (response.success) {
                $('#real-estate-modal .modal__content--success').show();
            } else {
                $('#real-estate-modal .modal__content--error').show();
            }

            $block.unblock();
        });
    });
});